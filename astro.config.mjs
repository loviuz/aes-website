import { defineConfig, sharpImageService } from "astro/config";
//import solidJs from "@astrojs/solid-js";
import tailwind from "@astrojs/tailwind";
import compress from "astro-compress";

import node from "@astrojs/node";
import deno from "@astrojs/deno";

// https://astro.build/config
export default defineConfig({
  integrations: [tailwind(), compress()],
  output: "static", // hybrid, dom, streaming

  compressHTML: true,

  experimental: {
    // hybridOutput: true,
    assets: true,
  },
  /*image: {
    service: sharpImageService(),
  },*/
  /*
  adapter: node({
    mode: "standalone", // or middleware
  }),
*/
  //adapter: deno(),
  /*vite: {
    ssr: {
      noExternal: ["path-to-regexp"],
    },
  },*/
  server: { port: 3000, host: true },
});

// currently we can't use bun since sharp only supports node runtime
