export const languages = {
  en: "English",
  it: "Italiano",
};

export const ui = {
  en: {
    "404.description": "Page not found",
    "404.homepage": "Home page",
    "header.lang": "English",
    "content.lobby-alt": "Our lobby",
    "content.features": "Features",
    "content.ui-bold": "Clean UI with small resources. ",
    "content.ui":
      "The Soothing32 palette grants a smooth and colourful experience in a lightweight package.",
    "content.minigames-bold": "Multiple minigames. ",
    "content.minigames":
      "On AES you will find many minigames that you can play with your friends.",
    "content.skins-bold": "Skins. ",
    "content.skins":
      "Customize your look with the 6 base skins available and unlock new ones by completing challenges!",
    "content.skins-alt": "Our skins selector",
    "content.parties-bold": "Join a party. ",
    "content.parties":
      "Use the /party command to create a party and invite your friends to play with you!",

    "content.support-us": "How you can support us",
    "content.donate-bold": "Donate something. ",
    "content.donate":
      "Thanks to LiberaPay you can support us economically in a simple and safe way. Even anonymously if you wish!",
    "content.donate-button": "Donate",
    "content.week": "every week",
    "content.play-on-the-server-bold": "Come play on our server. ",
    "content.play-on-the-server":
      "Starting to play is very simple, download Minetest and search for us in the server list!",
    "content.talk-about-us-to-friends-bold": "Talk about us with your friends.",
    "content.talk-about-us-to-friends":
      "The more we are, the more fun we have!",
    "footer.opendata": "Open data",
  },
  it: {
    "404.description": "Pagina non trovata",
    "404.homepage": "Pagina principale",
    "header.lang": "Italiano",
    "content.lobby-alt": "La nostra isola principale",
    "content.features": "Funzionalità",
    "content.ui-bold": "Interfaccia pulita che consuma poche risorse. ",
    "content.ui":
      "La palette Soothing32 garantisce un'esperienza fluida e colorata in un pacchetto leggero.",
    "content.minigames-bold": "Svariati minigiochi. ",
    "content.minigames":
      "Su AES troverai molti minigiochi che potrai giocare con i tuoi amici.",
    "content.skins-bold": "Skin. ",
    "content.skins":
      "Personalizza il tuo aspetto con le 6 skin base disponibili e sbloccane di nuove completando le sfide!",
    "content.skins-alt": "Il nostro selettore di skin",
    "content.parties-bold": "Gioca con i tuoi amici. ",
    "content.parties":
      "Usa il comando /party per creare un party e invitare i tuoi amici a giocare con te!",
    "content.support-us": "Come puoi sostenerci",
    "content.donate-bold": "Donaci qualcosa. ",
    "content.donate":
      "Grazie a LiberaPay puoi sostenerci economicamente in modo semplice e sicuro. Anche anonimamente se lo desideri!",
    "content.donate-button": "Dona",
    "content.week": "alla settimana",
    "content.play-on-the-server-bold": "Vieni a giocare sul nostro server. ",
    "content.play-on-the-server":
      "Iniziare a giocare è semplicissimo, scarica Minetest e cercaci nella lista dei server!",
    "content.talk-about-us-to-friends-bold": "Parlane con i tuoi amici.",
    "content.talk-about-us-to-friends": "Più siamo, più ci divertiamo!",
    "footer.opendata": "Dati aperti",
  },
} as const;
