/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      keyframes: {
        rainbow: {
          "0%": {
            color: "rgba(255, 0, 0, 1)",
          },
          "33%": {
            color: "rgba(255, 255, 0, 1)",
          },
          "66%": {
            color: "rgba(0, 192, 255, 1)",
          },
          "100%": {
            color: "rgba(192, 0, 255, 1)",
          },
        },
        pulse: {
          "0%": {
            scale: "1",
          },

          "100%": {
            scale: "1.05",
          },
        },
      },
      animation: {
        rainbow: "rainbow 2s linear infinite alternate",
        pulse: "pulse 0.5s linear infinite alternate",
      },
      fontFamily: {
        dogicapixel: ["dogicapixel", "regular"],
        dogicapixelbold: ["dogicapixelbold", "regular"],
      },
      colors: {
        "bg-light": "#fff",
        "text-light": "#171717",
        "text-secondary-light": "#737373",
        "menu-background-light": "#fff",
        "menu-background-hover-light": "#f7f7f7",
        "link-light": "#2563eb",
        "text-footer-light": "#737373",
        "text-on-yellow-light": "#171717",
        "liberapay-yellow-light": "#f6c915",

        "bg-dark": "#333",
        "text-dark": "#ededed",
        "text-secondary-dark": "#8b8b8b",
        "menu-background-dark": "#333",
        "menu-background-hover-dark": "#525252",
        "link-dark": "#93c5fd",
        "text-footer-dark": "#8b8b8b",
        "text-on-yellow-dark": "#171717",
        "liberapay-yellow-dark": "#f6c915",

        "bg-zughy32": "#f4cca1",
        "text-zughy32": "#302c2e",
        "text-secondary-zughy32": "#5a5353",
        "menu-background-zughy32": "#f47e1b",
        "menu-background-hover-zughy32": "#a0938e",
        "link-zughy32": "#3978a8",
        "text-footer-zughy32": "#5a5353",
        "text-on-yellow-zughy32": "#171717",
        "liberapay-yellow-zughy32": "#f6c915",
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
