module.exports = {
  plugins: [
    require("autoprefixer"),
    require("cssnano"),
    require("tailwindcss"),
    require("postcss-import"),
  ],
};
