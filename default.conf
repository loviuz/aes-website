server {
    listen 8081 default_server deferred;
    listen [::]:8081 default_server deferred;
    root /usr/share/nginx/html;  # Change this to the actual path of your static files
    # server_name  aes.land www.aes.land;

    location / {
        set $lang_cookie $cookie_lang;
        set $theme_cookie $cookie_theme;

        if ($lang_cookie = "") {
            return 302 /en;  # Default language if no lang cookie set
        }

        if ($theme_cookie = "") {
            set $theme_suffix "";
        }

        if ($theme_cookie = "dark") {
            set $theme_suffix "dark";
        }

        if ($theme_cookie = "light") {
            set $theme_suffix "light";
        }

        rewrite ^ /$lang_cookie/$theme_suffix permanent;
    }

    # Serve _astro css and various files
    location ~ ^/_astro/(.*) {
        root /usr/share/nginx/html;
        try_files /_astro/$1 =404;
    }

    location ~ ^/fonts/(.*) {
        add_header Access-Control-Allow-Origin *;
        root /usr/share/nginx/html;
        try_files /fonts/$1 =404;
    }

    location ~ ^/(it|en)/(dark|light) {
        add_header Set-Cookie "lang=$1; Path=/";
        add_header Set-Cookie "theme=$2; Path=/";
        try_files $uri $uri/ $uri/index.html =404;
    }

    location ~ ^/(it|en)/(.+) {
        set $lang $1;
        add_header Set-Cookie "lang=$lang; Path=/";

        if ($cookie_theme = "dark") {
            return 302 /$lang/dark/404;
        }

        if ($cookie_theme = "light") {
            return 302 /$lang/light/404;
        }

        try_files $uri/index.html =404;
    }

    location ~ ^/(it|en) {
        set $lang $1;

        if ($cookie_theme = "dark") {
            return 302 /$lang/dark;
        }

        if ($cookie_theme = "light") {
            return 302 /$lang/light;
        }

        add_header Set-Cookie "lang=$lang; Path=/";
        try_files $uri/index.html =404;
    }

    location ~ ^/(.+) {
        set $lang_cookie $cookie_lang;
        set $theme_cookie $cookie_theme;

        if ($lang_cookie = "") {
            return 302 /en/404;  # Default language if no lang cookie set
        }

        if ($theme_cookie = "") {
            set $theme_suffix "";
        }

        if ($theme_cookie = "dark") {
            set $theme_suffix "dark";
        }

        if ($theme_cookie = "light") {
            set $theme_suffix "light";
        }

        rewrite ^ /$lang_cookie/$theme_suffix/404 permanent;
    }

    

    # Custom error handling for 404 responses
    error_page 404 /404;

    location = /404 {
        internal;
        # redirect to the appropriate theme and lang
        set $lang_cookie $cookie_lang;
        set $theme_cookie $cookie_theme;

        if ($lang_cookie = "") {
            return 302 /en;  # Default language if no lang cookie set
        }

        if ($theme_cookie = "") {
            set $theme_suffix "";
        }

        if ($theme_cookie = "dark") {
            set $theme_suffix "dark";
        }

        if ($theme_cookie = "light") {
            set $theme_suffix "light";
        }

        rewrite ^ /$lang_cookie/$theme_suffix/404 permanent;
    }
}
