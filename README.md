# A.E.S. - Website

I recommend you use pnpm instead of npm:

pnpm install
pnpm run dev

To build the site:

pnpm run build

To preview the site after the build:

pnpm run preview

## Licenses

### Images

[src/assets/logo.webp](./src/assets/logo.webp) - CC-BY-SA 4.0 - [Zughy](https://gitlab.com/marco_a) 

[src/assets/social_logos/Liberapay.svg](./src/assets/social_logos/Liberapay.svg) - CC0 1.0

[src/assets/aes.png](./src/assets/aes.png) - CC-BY-SA 4.0 - [Zughy](https://gitlab.com/marco_a) 

[src/assets/skins.png](./src/assets/skins.png) - CC-BY-SA 4.0 - [\_Zaizen_](https://gitlab.com/_Zaizen_) 

### Fonts

[public/fonts/dogicapixel*](./public/fonts/) - OFL

### Source code

[AGPL 3.0](./LICENSE.txt)


### Reverse proxy

If you are using nginx proxy manager your proxy should look somethink like this:

```
location / {
        set $lang_cookie $cookie_lang;
        set $theme_cookie $cookie_theme;

add_header Cache-Control 'no-store';
  add_header Cache-Control 'no-cache';
  expires -1;

        if ($lang_cookie = "") {
            return 302 /en;  # Default language if no lang cookie set
        }

        if ($theme_cookie = "") {
            set $theme_suffix "";
        }

        if ($theme_cookie = "dark") {
            set $theme_suffix "dark";
        }

        if ($theme_cookie = "light") {
            set $theme_suffix "light";
        }

        rewrite ^ /$lang_cookie/$theme_suffix permanent;
    }

    # Serve _astro css and various files
    location ~ ^/_astro/(.*) {
 #add_header Access-Control-Allow-Origin *;
      
        proxy_pass http://WWW_AES_LAND-nginx:80/_astro/$1;
    }

    location ~ ^/fonts/(.*) {
       # add_header Access-Control-Allow-Origin *;

       proxy_pass http://WWW_AES_LAND-nginx:80/fonts/$1;
    }

    location ~ ^/(it|en)/(dark|light) {
add_header Cache-Control 'no-store';
  add_header Cache-Control 'no-cache';
  expires -1;
        add_header Set-Cookie "lang=$1; Path=/; Max-Age=63072000; SameSite=Strict;  HttpOnly; Secure";
        add_header Set-Cookie "theme=$2; Path=/; Max-Age=63072000; SameSite=Strict;  HttpOnly; Secure";
  proxy_pass http://WWW_AES_LAND-nginx:80/$request_uri/index.html;
        #try_files $uri $uri/ $uri/index.html =404;
    }

    location ~ ^/(it|en)/(.+) {
add_header Cache-Control 'no-store';
  add_header Cache-Control 'no-cache';
  expires -1;
        set $lang $1;
        add_header Set-Cookie "lang=$lang; Path=/; Max-Age=63072000; SameSite=Strict;  HttpOnly; Secure";

        if ($cookie_theme = "dark") {
            return 302 /$lang/dark/404;
        }

        if ($cookie_theme = "light") {
            return 302 /$lang/light/404;
        }
  proxy_pass http://WWW_AES_LAND-nginx:80/$request_uri/index.html;
       # try_files $uri/index.html =404;
    }

    location ~ ^/(it|en) {
add_header Cache-Control 'no-store';
  add_header Cache-Control 'no-cache';
  expires -1;
        set $lang $1;

        if ($cookie_theme = "dark") {
            return 302 /$lang/dark;
        }

        if ($cookie_theme = "light") {
            return 302 /$lang/light;
        }

        add_header Set-Cookie "lang=$lang; Path=/; Max-Age=63072000; SameSite=Strict; HttpOnly; Secure";
  proxy_pass http://WWW_AES_LAND-nginx:80/$request_uri/index.html;      
#  try_files $uri/index.html =404;
    }

    location ~ ^/(.+) {
        set $lang_cookie $cookie_lang;
        set $theme_cookie $cookie_theme;

        if ($lang_cookie = "") {
            return 302 /en/404;  # Default language if no lang cookie set
        }

        if ($theme_cookie = "") {
            set $theme_suffix "";
        }

        if ($theme_cookie = "dark") {
            set $theme_suffix "dark";
        }

        if ($theme_cookie = "light") {
            set $theme_suffix "light";
        }

        rewrite ^ /$lang_cookie/$theme_suffix/404 permanent;
    }

    

    # Custom error handling for 404 responses
    error_page 404 /404;

    location = /404 {
        internal;
        # redirect to the appropriate theme and lang
        set $lang_cookie $cookie_lang;
        set $theme_cookie $cookie_theme;

        if ($lang_cookie = "") {
            return 302 /en;  # Default language if no lang cookie set
        }

        if ($theme_cookie = "") {
            set $theme_suffix "";
        }

        if ($theme_cookie = "dark") {
            set $theme_suffix "dark";
        }

        if ($theme_cookie = "light") {
            set $theme_suffix "light";
        }

        rewrite ^ /$lang_cookie/$theme_suffix/404 permanent;
    }
```