FROM node:20.5-alpine3.17

# Copy the files to /app

WORKDIR /app

# enable corepack
RUN corepack enable

RUN corepack prepare pnpm@latest --activate
RUN apk add --upgrade --no-cache vips-dev build-base --repository https://alpine.global.ssl.fastly.net/alpine/v3.10/community/
#CMD ["pnpm", "install", "pnpm", "run", "dev"]

# copy package json
COPY package.json /app/package.json

RUN npm install --unsafe-perm

CMD pnpm install sharp && pnpm install && pnpm run build 
